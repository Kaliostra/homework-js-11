"use strict";



const psForm = document.querySelector("form");

function showPassword() {
    let inputs = document.querySelectorAll(".js-input");

    function iconClick(target,n) {
        for (let input of inputs) {
            if (input.getAttribute('type') == 'password') {
                target.classList.add('fa-eye-slash');
                input.setAttribute('type', 'text');
            }
             else {
                target.classList.remove('fa-eye-slash');
                input.setAttribute('type', 'password');
            }
        }
        
    }
    psForm.addEventListener("click", (event) => {
        if (event.target.classList.contains("fas")){
            iconClick(event.target);
        }
    });
}

function onClick() {
    let firstInput = document.getElementById("password-input-one");
    let secondInput = document.getElementById("password-input-two");
    let error = document.getElementById("error");
    if (firstInput.value != secondInput.value || firstInput.value == "") {
        error.classList.remove("notError");
        error.classList.add("error")
        return false;
    } else {
        error.classList.remove("error")
        error.classList.add("notError");
        alert("You are welcome");
        return true;
    }
}

showPassword();
 